package com.ds;

import com.ds.model.Mail;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.stream.Field;

public class CustomContentHandler extends AbstractContentHandler {
    private Mail mail;

    public CustomContentHandler(Mail mail) {
        this.mail = mail;
    }


    @Override
    public void field(Field field) {
        switch (field.getName()) {
            case "From":
                mail.setFrom(field.getBody());
                break;
            case "To":
                mail.setTo(field.getBody());
                break;
            case "X-DSPAM-Probability":
                mail.setxDSPAMProbability(Double.valueOf(field.getBody()));
                break;
            case "X-DSPAM-Confidence":
                mail.setxDSPAMConfidence(Double.valueOf(field.getBody()));
                break;
        }
    }
}
