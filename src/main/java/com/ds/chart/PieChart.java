package com.ds.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class PieChart extends ApplicationFrame {
    public PieChart(String title, HashMap<String, Integer> data) {
        super(title);
        JFreeChart chart = ChartFactory.createPieChart(
                title,   // chart title
                createDataset(data),
                false,
                false,
                false
        );

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(1000, 500));
        setContentPane(chartPanel);
    }

    public static PieDataset createDataset(HashMap<String, Integer> data) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        data.forEach(dataset::setValue);
        return dataset;
    }


}
