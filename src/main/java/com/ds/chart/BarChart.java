package com.ds.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.*;
import java.util.HashMap;

public class BarChart extends ApplicationFrame {
    public BarChart(String title, HashMap<String, Integer> data) {
        super(title);

        JFreeChart barChart = ChartFactory.createBarChart(
                title,
                "Senders",
                "Count",
                createDataset(data),
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new Dimension(1000, 500));
        setContentPane(chartPanel);
    }

    private CategoryDataset createDataset(HashMap<String, Integer> data) {


        final DefaultCategoryDataset dataset =
                new DefaultCategoryDataset();
        data.forEach((s, integer) -> dataset.addValue(integer, s, ""));

        return dataset;
    }
}
